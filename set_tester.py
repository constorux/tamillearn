from random import randint

dictionary = []
test_mode = 1
test_lengh = 10

print("TestMe, (c) Robin Naumann 2019")
print("------------------------------")
file = input("dictionary file: ")
with open(file) as f:
	for line in f:
		entries = [[]]
		line.replace('\n', '').replace('\r', '')
		entries.extend([x.strip() for x in line.split(',')])
		dictionary.append(entries)

test_mode =  int(input("test mode  : "))
test_lengh = int(input("test lengh : "))
print("------------------------------")

i = 0
while i < test_lengh:
	entry_nr = randint(0, len(dictionary) - 1)
	if dictionary[entry_nr][test_mode] != "" and dictionary[entry_nr][test_mode] != "none":
		print(" --> " + "\033[1m" + dictionary[entry_nr][test_mode] + "\033[0m")
		
		if input(" <-- ") == dictionary[entry_nr][1]:
			dictionary[entry_nr][0].append(1)
			print("\r\n" + "\033[92m" + "correct!" + "\033[0m")
		else:
			dictionary[entry_nr][0].append(0)
			print("\r\n" + "\033[93m" + "wrong! correct answer: " + "\033[0m"
			"\033[1m" + dictionary[entry_nr][1] + "\033[0m")
		print("------------------------------")
		
		i += 1
print("Test completed :D")
print("Results: ")
for vocab in dictionary:
	bar = ""
	if len(vocab[0]) > 0:
		percentage = round(sum(vocab[0])/float(len(vocab[0])) * 100)
		i = 0
		while i < percentage:
			bar = bar + "-"
			i += 2
		bar = str(percentage).ljust(3) + "% " + "\033[36m" + bar + "\033[0m"
	
	print("\033[1m" + "     " + vocab[test_mode].ljust(9) + "\033[0m"
	+ "    | " + vocab[1].ljust(5) + "    " + bar)
		

